package vault

import (
	"testing"
	"golang.org/x/net/context"
)

func TestHasherService(t *testing.T) {
	// create new service
	srv := NewService()
	ctx := context.Background()
	h, err := srv.Hash(ctx, "password")
	if err != nil {
		t.Errorf("Hash: %s", err)
	}
	// validate with valid password
	ok, err := srv.Validate(ctx, "password", h)
	if err != nil {
		t.Errorf("Valid: %s", err)
	}
	if !ok {
		t.Error("Expected true from valid")
	}
	// try validate with wrong password
	ok, err = srv.Validate(ctx, "wrong password", h)
	if err != nil {
		t.Errorf("Valid: %s", err)
	}
	if ok {
		t.Error("Expected false from Valid")
	}
}