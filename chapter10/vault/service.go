package vault

import (
	"golang.org/x/net/context"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"encoding/json"
)

// service provides password hashing capabilities
type Service interface {
	Hash(ctx context.Context, password string) (string, error)
	Validate(ctx context.Context, password, hash string) (bool, error)
}

// This is a go'tric to group methods together in order to implement `Service` interface without storing any state
type vaultService struct {}

// NewService makes a new Service
func NewService() Service {
	return vaultService{}
}

// Implementing `Service` interface for `vaultService` struct.
// Hash used to generate hash from provided password
func (vaultService) Hash(ctx context.Context, password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// Validate to check whether matching a password and a hash
func (vaultService) Validate(ctx context.Context, password, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, nil
	}
	return true, nil
}

// Modeling request and response message
type hashRequest struct {
	Password string `json:"password"`
}
type hashResponse struct {
	Hash string `json:"hash"`
	Err string `json:"err,omitempty"`
}

// helper method  to help decode JSON body
func decodeHashRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req hashRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}