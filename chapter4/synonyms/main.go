package main

import (
	"os"
	"bitbucket.org/thanhngvpt/goblueprint/chapter4/thesaurus"
	"bufio"
	"log"
	"fmt"
)

func main() {
	apiKEY := os.Getenv("BHT_APIKEY")
	thesaurus := &thesaurus.BigHuge{APIKey: apiKEY}
	s := bufio.NewScanner(os.Stdin)
	//fmt.Print("Enter a word to lookup synonyms: ");
	for s.Scan() {
		word := s.Text()
		syns, err := thesaurus.Synonyms(word)
		if err != nil {
			log.Fatalln("Failed when looking for synonyms for " + word, err)
		}
		if len(syns) == 0 {
			log.Fatalln("Couldn't find any synonyms for " + word)
		}
		//fmt.Println("---------- Synonyms for " + word + " -----------")
		for _, syn := range syns {
			fmt.Println(syn)
		}
	}
}
