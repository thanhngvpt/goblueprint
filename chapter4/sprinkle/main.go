package main

import (
	"math/rand"
	"time"
	"bufio"
	"os"
	"fmt"
	"strings"
)

const otherWord = "*"
var transforms = []string {
	otherWord,
	otherWord + " app",
	otherWord + " site",
	otherWord + " time",
	"get " + otherWord,
	"go " + otherWord,
	"lets " + otherWord,
	otherWord + "hq",
}

func main() {
	// run rand.Seed() to provide a salt for random algorithm, this ensure every program running will have
	// a differ rand that random algorithm will rely on
	rand.Seed(time.Now().UTC().UnixNano())
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		// get random element from transforms slice (word by word random in range 0 to len of transforms)
		t := transforms[rand.Intn(len(transforms))]
		fmt.Println(strings.Replace(t, otherWord, s.Text(), -1))
	}
}
