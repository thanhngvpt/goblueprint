package main

import (
	"math/rand"
	"time"
	"bufio"
	"os"
	"fmt"
)

const (
	duplicateVowel bool = true
	removeVowel bool = false
)

func randBool() bool {
	return rand.Intn(2) == 0
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		word := []byte(s.Text())
		// randomly to decide whether to mutate `word` or not
		if randBool() {
			var vI int = -1
			for i, char := range word {
				switch char {
				case 'u', 'e', 'o', 'a', 'i', 'U', 'E', 'O', 'A', 'I':
					// randomly to decide whether to save vowel index or not
					if randBool() {
						vI = i
					}
				}
			}
			if vI >= 0 {
				// randomly to choice the way to mutate `word`
				switch randBool() {
				case duplicateVowel:
					word = append(word[:vI+1], word[vI:]...)
				case removeVowel:
					word = append(word[:vI], word[vI+1:]...)
				}
			}
		}
		fmt.Println(string(word))
	}
}