package main

import (
	"net/http"
	"log"
	"sync"
	"text/template"
	"path/filepath"
	"flag"
	"github.com/stretchr/gomniauth"
	"github.com/stretchr/gomniauth/providers/facebook"
	"github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/gomniauth/providers/github"
	"github.com/stretchr/objx"
)

// temp1 represents a single template
type templateHandler struct {
	once sync.Once
	filename string
	templ *template.Template
}

// ServerHTTP handles the HTTP requests
func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})
	data := map[string]interface{}{
		"Host": r.Host,
	}
	if authCookie, err := r.Cookie("auth"); err == nil {
		data["UserData"] = objx.MustFromBase64(authCookie.Value)
	}
	t.templ.Execute(w, data)
}

func main() {
	// get host configurable
	var addr = flag.String("addr", ":8818", "The addr of the application")
	// parse the flags
	flag.Parse()

	// Setup gomniauth
	gomniauth.SetSecurityKey("112fbg7iu2nb4uywevihjw4tuiyub34noilk")
	gomniauth.WithProviders(
		facebook.New(
			"1833920070218958",
			"f1330588696c6052938786b5eac59eda",
			"http://localhost:8818/auth/callback/facebook",
		),
		google.New(
			"102379851567-me4sgd6pnk0s6bkv9kscofkt7bef2pl2.apps.googleusercontent.com",
			"UlXdyF9V-QTq93RuJeEwsfRJ",
			"http://localhost:8818/auth/callback/google",
		),
		github.New(
			"17ea8762e3a7587a39df",
			"8c55a5953e0e15d1c69b88d0855ea7c83e0096ca",
			"http://localhost:8818/auth/callback/github",
		),
	)

	// create a room
	r := newRoom()

	// Routes
	// Serve public media
	http.Handle("/asserts/", http.StripPrefix("/asserts", http.FileServer(http.Dir("asserts"))))

	http.Handle("/chat", MustAuth(&templateHandler{filename: "chat.html"}))
	http.Handle("/login", &templateHandler{filename: "login.html"})
	http.HandleFunc("/auth/", loginHandler)

	http.Handle("/room", r)


	// get the room going
	go r.run()

	// start the web server
	log.Println("Starting web server on: ", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe Error: ", err)
	}
}