package main

import (
	"errors"
	"io/ioutil"
	"path"
)

// ErrNoAvatarURL is the error that is returned when the avatar instance is unable to provide an avatar url.
var ErrNoAvatarURL = errors.New("chat: Unable to get an avatar url")

// Avatar represents types capable of representing user profile pictures
type Avatar interface {
	// GetAvatarURL gets the avatar url for the specified client or returns an error if something goes wrong.
	// ErrNoAvatarURL is returned if the object is unable to get a URL for the specified client.
	GetAvatarURL(ChatUser) (string, error)
}

// TryAvatars is a slice of Avatar
type TryAvatars []Avatar

// GetAvatarURL implement Avatar interface
func (a TryAvatars) GetAvatarURL(u ChatUser) (string, error) {
	for _, avatar := range a {
		if url, err := avatar.GetAvatarURL(u); err == nil {
			return url, nil
		}
	}
	return "", ErrNoAvatarURL
}

// AuthAvatar struct
type AuthAvatar struct {
}

// UseAuthAvatar var
var UseAuthAvatar AuthAvatar

// GetAvatarURL is implemented Avatar interface
func (_ AuthAvatar) GetAvatarURL(u ChatUser) (string, error) {
	url := u.AvatarURL()
	if len(url) > 0 {
		return url, nil
	}
	return "", ErrNoAvatarURL
}

// GravatarAvatar struct
type GravatarAvatar struct {
}

// UseGravatarAvatar var
var UseGravatarAvatar GravatarAvatar

// GetAvatarURL is implemented Avatar interface
func (_ GravatarAvatar) GetAvatarURL(u ChatUser) (string, error) {
	return "//www.gravatar.com/avatar/" + u.UniqueID(), nil
}

// FileSystemAvatar struct
type FileSystemAvatar struct {
}

// UseFileSystemAvatar var
var UseFileSystemAvatar FileSystemAvatar

// GetAvatarURL is implemented Avatar interface
func (_ FileSystemAvatar) GetAvatarURL(u ChatUser) (string, error) {
	if files, err := ioutil.ReadDir("avatars"); err == nil {
		for _, file := range files {
			if file.IsDir() {
				continue
			}
			if match, _ := path.Match(u.UniqueID()+"*", file.Name()); match {
				return "/avatars/" + file.Name(), nil
			}
		}
	}
	return "", ErrNoAvatarURL
}
